/**
 * Bài 1:
 */

function tinh() {
    const aEl = document.getElementById('aEl').value * 1;
    const bEl = document.getElementById('bEl').value * 1;
    const cEl = document.getElementById('cEl').value * 1;

    if (aEl>bEl && aEl>cEl) {
        if (bEl>cEl) {
            document.getElementById("result1").innerHTML = 
            cEl + "," + bEl + "," + aEl;
        } 
        else {
            document.getElementById("result1").innerHTML =
            bEl + "," + cEl + "," + aEl;
        }
    }
    else if (bEl>aEl && bEl>cEl) {
        if (aEl>cEl) {
            document.getElementById("result1").innerHTML = 
            cEl + "," + aEl + "," + bEl;
        } else {
            document.getElementById("result1").innerHTML =
            aEl + "," + cEl + "," + bEl;
        }
    }
    else if (cEl>aEl && cEl>bEl) {
        if (aEl>bEl) {
            document.getElementById("result1").innerHTML = 
            bEl + "," + aEl + "," + cEl;
        } else {
            document.getElementById("result1").innerHTML =
            aEl + "," + bEl + "," + cEl;
        }
    }
}

/**
 * Bài 2
 */

function chao() {
    var thanhVien = document.getElementById("chon");
    if (thanhVien.value == 5) {
        document.getElementById("result2").innerHTML = `Hello`;
    }
    else {
        var User = thanhVien.options[thanhVien.selectedIndex].text;
        document.getElementById("result2").innerHTML = `Hello ${User}`;
    }
}

/**
 * bài 3
 */
function xuat() {
    var xEl = document.getElementById('xEl').value * 1;
    var yEl = document.getElementById('yEl').value * 1;
    var zEl = document.getElementById('zEl').value * 1;

    var soLuongSoChan = 0;
    var soLuongSoLe = 0;

    if (xEl % 2 ==0) {
        soLuongSoChan++;
    }
    if (yEl % 2 ==0) {
        soLuongSoChan += 1;
    }
    if (zEl % 2 ==0) {
        soLuongSoChan += 1;
    }
    
    soLuongSoLe = 3 - soLuongSoChan;
    document.getElementById("result3").innerHTML =`<p>Số chẵn: ${soLuongSoChan}, Số lẻ: ${soLuongSoLe}</p>`
}

/**
 * bài 4
 */

function ketQua() {
    var a = document.getElementById('canh-a').value * 1;
    var b = document.getElementById('canh-b').value * 1;
    var c = document.getElementById('canh-c').value * 1;

    if ((a + b > c) && (a + c > b) && (b + c > a) && (a > 0) && (b > 0) && (c > 0)) {
        if ((a == b) && (b == c)) {
            document.getElementById("result4").innerHTML = `Tam giác đều`
        } 
        else if ((a == b) || (a == c) || (b == c)) {
                document.getElementById("result4").innerHTML = `Tam giác cân`
        } 
        else if ((a * a == b * b + c * c) || 
                (b * b == a * a + c * c) ||
                (c * c == a * a + b * b)) {
                document.getElementById("result4").innerHTML = `Tam giác vuông`
        } 
        else if (((a * a == b * b + c * c) && (b == c)) ||
                ((b * b == a * a + c * c) && (a == c)) ||
                ((c * c == a * a + b * b) && (a == b))) {
                document.getElementById("result4").innerHTML = `Tam giác vuông cân`
        } 
        else {
            document.getElementById("result4").innerHTML = `Tam giác thường`
            }
        } 
        else {
            document.getElementById("result4").innerHTML = `3 cạnh của tam giác không thỏa mãn điều kiện lập thành tam giác!`
        }
}